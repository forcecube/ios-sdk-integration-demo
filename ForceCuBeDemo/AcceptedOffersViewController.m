//
//  FCBAcceptedOffersViewController.m
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 01/02/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

#import "AcceptedOffersViewController.h"

@interface AcceptedOffersViewController () <FCBAcceptedOfferViewControllerDelegate>

@property NSArray * acceptedOffers;

@end

@implementation AcceptedOffersViewController

//**************************************************************************************************
- (void) viewWillAppear: (BOOL) animated
{
    self.acceptedOffers = [self.forcecube.campaignManager acceptedOffers];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

//**************************************************************************************************
- (NSInteger) tableView: (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
    return self.acceptedOffers.count;
}

//**************************************************************************************************
- (UITableViewCell *) tableView: (UITableView *) tableView cellForRowAtIndexPath: (NSIndexPath *) indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier: @"AAA"];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleSubtitle
                                      reuseIdentifier: @"AAA"];
    }
    
    id<FCBCampaignOffer> offer = self.acceptedOffers[indexPath.row];
    
    
    __weak UITableViewCell * weakCell = cell;
    [self loadImageFromURL: offer.imageUrl completion: ^(UIImage *image) {
        if (image)
            weakCell.imageView.image = image;
    }];
    
    cell.textLabel.text = offer.fullscreenTitle;
    cell.detailTextLabel.text = offer.fullscreenText;
    
    return cell;
}


#pragma mark - Table view delegate

//**************************************************************************************************
- (void)tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath
{
    id<FCBCampaignOffer> offer = self.acceptedOffers[indexPath.row];

    FCBAcceptedOfferViewController * acceptedOfferVC = [self.forcecube acceptedOfferControllerWithCampaignId: offer.campaignOfferId];
    
    acceptedOfferVC.delegate = self;
    
    [self.navigationController pushViewController: acceptedOfferVC animated:YES];
}

//**************************************************************************************************
- (void) acceptedOfferViewControllerDidComplete: (FCBAcceptedOfferViewController *) controller
                                didCheckInOffer: (BOOL) didCheckInOfffer
{
    [self.navigationController popViewControllerAnimated: YES];
}


//**************************************************************************************************
- (void) loadImageFromURL: (NSURL*) url completion: (void(^)(UIImage * image)) completion
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSError * err = nil;
        NSData * data = [NSData dataWithContentsOfURL: url options: 0 error: &err];
        
        if (err)
        {
            completion(nil);
            return;
        }

        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (data)
            {
                UIImage * image = [UIImage imageWithData: data];
                completion(image);
            }
            else
                completion(nil);
        });
    });
}

@end
